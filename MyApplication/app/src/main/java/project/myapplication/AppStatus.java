package project.myapplication;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

public class AppStatus {

    static Context context;
    /**
     * We use this class to determine if the application has been connected to either WIFI Or Mobile
     * Network, before we make any network request to the server.
     * <p>
     * The class uses two permission - INTERNET and ACCESS NETWORK STATE, to determine the user's
     * connection stats
     */

    private static AppStatus instance = new AppStatus();
    ConnectivityManager connectivityManager;
    NetworkInfo wifiInfo, mobileInfo;
    boolean connected = false;
    boolean isWifiConn = false ;
    boolean isMobileConn = false;

    //NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

    //final boolean wifi = networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected() && networkInfo.getType() == connectivityManager.TYPE_WIFI;

    //final boolean mobile = mobileInfo.getType() == connectivityManager.TYPE_MOBILE;

    public static AppStatus getInstance(Context ctx) {
        context = ctx.getApplicationContext();
        return instance;
    }

    public boolean isOnline() {
        try {

            connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            isWifiConn = networkInfo.isConnected();
            if(isWifiConn){return isWifiConn;}
/*
            String ssid = "none";
            WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            assert wifiManager != null;
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            if (WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState()) == NetworkInfo.DetailedState.CONNECTED) {
                ssid = wifiInfo.getSSID();
            }

            String signs = ssid.substring(0,2);
            Log.v("first 2 signs", ssid);


*/

            networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            isMobileConn = networkInfo.isConnected();
            if(isMobileConn){ return isMobileConn;}



            // return connected;


        } catch (Exception e) {
            System.out.println("CheckConnectivity Exception: " + e.getMessage());
            Log.v("connectivity", e.toString());
        }
        return connected;



    }

    public String getWifiName(Context context) {

        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        assert wifiManager != null;
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        String ssid = wifiInfo.getSSID();
        String signs = ssid.substring(1,3);
        System.out.println("........................................ dupa " + signs);

        Log.v("first 2 signs", signs);

        return signs;
    }





}