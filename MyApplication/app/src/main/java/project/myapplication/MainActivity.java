package project.myapplication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.AWSStartupHandler;
import com.amazonaws.mobile.client.AWSStartupResult;
import com.amazonaws.mobileconnectors.pinpoint.PinpointConfiguration;
import com.amazonaws.mobileconnectors.pinpoint.PinpointManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Objects;

import static android.content.ContentValues.TAG;


public class MainActivity extends Activity {


    public static final String LOG_TAG = MainActivity.class.getSimpleName();


    public static PinpointManager pinpointManager;


    private ProgressBar spinner;
    String ShowOrHideWebViewInitialUse = "show";

    public String cellularURL = "http://www.reeffactory.com/portal";
   //public String cellularURL = "http://facebook.com";
    public String wifiUrl =  "http://192.168.1.1/index.html";
   //public String wifiUrl = "http://facebook.com";

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AWSMobileClient.getInstance().initialize(this, new AWSStartupHandler() {
            @Override
            public void onComplete(AWSStartupResult awsStartupResult) {
                Log.d("YourMainActivity", "AWSMobileClient is instantiated and you are connected to AWS!");
            }
        }).execute();




        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_main);


        if (AppStatus.getInstance(getApplicationContext()).isOnline()) {
            /**
             * Internet is available, Toast It!
             *
             */

            WebView webb = findViewById(R.id.web1);
            spinner = findViewById(R.id.progressBar);
            webb.setWebViewClient(new CustomWebViewClient());

            webb.getSettings().setJavaScriptEnabled(true);
            webb.getSettings().setDomStorageEnabled(true);
            webb.setOverScrollMode(WebView.OVER_SCROLL_NEVER);
            if (AppStatus.getInstance(getApplicationContext()).isWifiConn) {

                if(Objects.equals(AppStatus.getInstance(getApplicationContext()).getWifiName(getApplicationContext()), "RF")) {


                    webb.loadUrl(wifiUrl);
                    Toast.makeText(getApplicationContext(), "WiFi Networks Connected!", Toast.LENGTH_SHORT).show();
                }else{


                    webb.loadUrl(cellularURL);
                    Toast.makeText(getApplicationContext(), "WiFi Networks Connected!", Toast.LENGTH_SHORT).show();


                }


            } else if (AppStatus.getInstance(getApplicationContext()).isMobileConn) {


                webb.loadUrl(cellularURL);

                Toast.makeText(getApplicationContext(), "Mobile Networks Connected!", Toast.LENGTH_SHORT).show();
            }

        } else {
            /**
             * Internet is NOT available, Toast It!
             */

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage(R.string.brak_polaczenia);
            alertDialogBuilder.setPositiveButton(R.string.tak, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(intent);



                }
            });

            alertDialogBuilder.setNegativeButton(R.string.nie, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent = new Intent(MainActivity.this, CheckInternetActivity.class);
                    startActivity(intent);



                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();


            Toast.makeText(getApplicationContext(), "Ooops! No WiFi/Mobile Networks Connected!", Toast.LENGTH_SHORT).show();

        }

        if (pinpointManager == null) {
            PinpointConfiguration pinpointConfig = new PinpointConfiguration(
                    getApplicationContext(),
                    AWSMobileClient.getInstance().getCredentialsProvider(),
                    AWSMobileClient.getInstance().getConfiguration());

            pinpointManager = new PinpointManager(pinpointConfig);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String deviceToken =
                                FirebaseInstanceId.getInstance().getToken("arn:aws:sns:eu-central-1:002084392106:app/GCM/RfOnlineAndroid", FirebaseMessaging.INSTANCE_ID_SCOPE);
                        Log.i(TAG, "FCM Registration Token: " + deviceToken);
                        pinpointManager.getNotificationClient()
                                .registerGCMDeviceToken(deviceToken);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    private class CustomWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView webview, String url, Bitmap favicon) {

            if (ShowOrHideWebViewInitialUse.equals("show")) {

                webview.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {


            ShowOrHideWebViewInitialUse = "hide";
            spinner.setVisibility(View.GONE);

            view.setVisibility(View.VISIBLE);
            super.onPageFinished(view, url);

        }
    }

    /*@Override
    protected void onPause() {
        super.onPause();
        startActivity(getIntent());

    }
*/
    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

}

