package project.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by anetamalecka on 29.04.2018.
 */

public class CheckInternetActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_internet);


        Button refresh = findViewById(R.id.button);

        refresh.setOnClickListener(new View.OnClickListener() {
           @Override
            public void onClick(View v) {


                   Intent intent;
                   intent = new Intent(CheckInternetActivity.this, MainActivity.class);
                   startActivity(intent);



            }
        });


    }


    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }


}
